﻿namespace Evercraft

open System

type Ability =
    | Strength = 1
    | Dexterity = 2
    | Constitution = 3
    | Intelligence = 4
    | Wisdom = 5
    | Charisma = 6

type Alignment =
    | Evil
    | Neutral
    | Good

type Character =
    | Default
    | Named of string * Character
    | Aligned of Alignment * Character
    | WithHitPointAward of int * Character
    | WithAbilityScore of Ability * int * Character
    | WithExperiencePointAward of int * Character

module Character =
    let Create () : Character =
        Default

    let SetName (name:string) (character:Character) : Character =
        Named (name, character)

    let rec GetName (character:Character) : string =
        match character with
        | Named (name, _) ->
            name
        | Aligned (_, inner) ->
            inner |> GetName
        | WithHitPointAward (_, inner) ->
            inner |> GetName
        | WithAbilityScore (_, _, inner) ->
            inner |> GetName
        | WithExperiencePointAward (_, inner) ->
            inner |> GetName
        | Default ->
            ""
    
    let rec GetAlignment (character:Character) : Alignment =
        match character with
        | Named (_, inner) ->
            inner |> GetAlignment
        | Aligned (alignment, _) ->
            alignment
        | WithHitPointAward (_, inner) ->
            inner |> GetAlignment
        | WithAbilityScore (_, _, inner) ->
            inner |> GetAlignment
        | WithExperiencePointAward (_, inner) ->
            inner |> GetAlignment
        | Default ->
            Neutral

    let SetAlignment (alignment: Alignment) (character:Character) : Character =
        Aligned (alignment, character)

    let rec GetAbilityScore (ability:Ability) (character:Character) : int =
        match character with
        | WithAbilityScore (a, v, _) when a = ability ->
            v
        | WithAbilityScore (_, _, inner) ->
            inner |> GetAbilityScore ability
        | WithHitPointAward (_, inner) ->
            inner |> GetAbilityScore ability
        | Named (_, inner) ->
            inner |> GetAbilityScore ability
        | Aligned (_, inner) ->
            inner |> GetAbilityScore ability
        | WithExperiencePointAward (_, inner) ->
            inner |> GetAbilityScore ability
        | Default ->
            10

    let SetAbilityScore (ability:Ability) (score:int) (character:Character) : Character =
        WithAbilityScore (ability, score, character)

    let GetAbilityModifier (ability:Ability) (character: Character) =
        let abilityScore = 
            character
            |> GetAbilityScore ability
        ((abilityScore - 10) |> float) / 2.0 |> floor |> int

    let GetArmourClass (character:Character) : int =
        10 + (character |> GetAbilityModifier Ability.Dexterity)

    let private AwardHitPoints (award: int) (character:Character) : Character =
        WithHitPointAward (award, character)

    let AddExperiencePoints (experiencePoints:int) (character: Character) : Character =
        WithExperiencePointAward (experiencePoints, character)

    let rec GetExperiencePoints (character: Character) : int =
        match character with
        | WithHitPointAward (_, inner) ->
            inner
            |> GetExperiencePoints
        | Named (_,inner) ->
            inner 
            |> GetExperiencePoints
        | Aligned (_, inner) ->
            inner |> GetExperiencePoints
        | WithAbilityScore (_, _, inner) ->
            inner |> GetExperiencePoints
        | WithExperiencePointAward (experiencePoints, inner) ->
            experiencePoints + (inner |> GetExperiencePoints)
        | Default -> 0

    let GetExperienceLevel (character:Character) : int =
        (character |> GetExperiencePoints) / 1000 + 1

    let rec private GetHitPointsRecursive (originalCharacter:Character) (character:Character) : int =
        match character with
        | Aligned (_, inner) ->
            inner |> GetHitPointsRecursive originalCharacter
        | Named (_, inner) ->
            inner |> GetHitPointsRecursive originalCharacter
        | WithHitPointAward (award, inner) ->
            award + (inner |> GetHitPointsRecursive originalCharacter)
        | WithAbilityScore (_, _, inner) ->
            inner |> GetHitPointsRecursive originalCharacter
        | WithExperiencePointAward (_,inner) ->
            inner |> GetHitPointsRecursive originalCharacter
        | Default ->
            (max 1 (5 + (originalCharacter |> GetAbilityModifier Ability.Constitution))) * (originalCharacter |> GetExperienceLevel)

    let GetHitPoints (character:Character) : int =
        GetHitPointsRecursive character character

    let SetHitPoints (hitPoints:int) (character:Character) : Character =
        AwardHitPoints (hitPoints - (character |> GetHitPoints)) character

    let GetDeadness (character:Character) : bool =
        (character |> GetHitPoints) <= 0

    let internal GetAttackBonus (character : Character) : int =
        let levelAttackBonus = 
            (GetExperienceLevel character) / 2
        (character 
        |> GetAbilityModifier Ability.Strength) + levelAttackBonus

    let internal GetBaseDamage (character : Character) : int =
        max 1 (1+(character |> GetAbilityModifier Ability.Strength))

    let internal GetCriticalDamage (character : Character) : int =
        (GetBaseDamage character) * 2

