﻿namespace Evercraft

open System

module Attack =
    let Resolve
            (attacker : Character)            
            (defender : Character) 
            (dieRoll  : int)
            : bool * Character * Character =
        let attackRoll = dieRoll + (attacker |> Character.GetAttackBonus)
        let defence = defender |> Character.GetArmourClass
        if attackRoll >= defence then
            let damage = 
                if dieRoll = 20 then 
                    attacker |> Character.GetCriticalDamage 
                else 
                    attacker |> Character.GetBaseDamage
            let originalHitPoints = defender |> Character.GetHitPoints
            let updatedHitPoints = originalHitPoints - damage
            let updatedAttacker = 
                attacker
                |> Character.AddExperiencePoints 10
            let updatedDefender = 
                defender
                |> Character.SetHitPoints updatedHitPoints
            (true, 
                updatedAttacker,
                updatedDefender)
        else
            false, attacker, defender

