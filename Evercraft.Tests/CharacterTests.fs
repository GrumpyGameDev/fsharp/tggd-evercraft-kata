module CharacterTests

open Evercraft
open Xunit

[<Fact>]
let ``SetName and GetName.They set and get the character's name.`` () =
    let name = "Davin"
    let character = 
        Character.Create()
        |> Character.SetName name
    let expected = name
    let actual =
        character
        |> Character.GetName
    Assert.Equal(expected, actual)

[<Fact>]
let ``GetName.It defaults to empty string.`` () =
    let character =
        Character.Create()
    let expected = ""
    let actual =
        character
        |> Character.GetName
    Assert.Equal(expected, actual)

[<Fact>]
let ``GetAlignment.It defaults to Neutral.`` () =
    let character =
        Character.Create()
    let expected =
        Neutral
    let actual =
        character
        |> Character.GetAlignment
    Assert.Equal(expected, actual)

[<Fact>]
let ``SetAlignment.It sets the alignment of a character to Good`` () =
    let character =
        Character.Create()
        |> Character.SetAlignment Good
    let expected = Good
    let actual =
        character
        |> Character.GetAlignment
    Assert.Equal(expected, actual)

[<Fact>]
let ``GetArmourClass.It retrieves the armour class of the character.`` () =
    let character =
        Character.Create()
    let expected = 10
    let actual = 
        character
        |> Character.GetArmourClass
    Assert.Equal(expected, actual)

[<Fact>]
let ``GetHitPoints.It retrieves the hitpoint of the character.`` () =
    let character =
        Character.Create()
    let expected = 5
    let actual =
        character
        |> Character.GetHitPoints
    Assert.Equal(expected, actual)

[<Theory>]
[<InlineData(0, 0, true)>]
[<InlineData(1, 1, false)>]
[<InlineData(2, 2, false)>]
[<InlineData(3, 3, false)>]
[<InlineData(4, 4, false)>]
let ``SetHitPoints.It sets the hitpoints of the character.`` (givenHitPoints:int, expectedHitPoints:int, expectedDeadness:bool) =
    let character = 
        Character.Create()
        |> Character.SetHitPoints givenHitPoints
    let actualHitPoints =
        character
        |> Character.GetHitPoints
    Assert.Equal(expectedHitPoints, actualHitPoints)
    let actualDeadness =
        character
        |> Character.GetDeadness
    Assert.Equal(expectedDeadness, actualDeadness)

[<Theory>]
[<InlineData(Ability.Strength, 10)>]
[<InlineData(Ability.Dexterity, 10)>]
[<InlineData(Ability.Constitution, 10)>]
[<InlineData(Ability.Intelligence, 10)>]
[<InlineData(Ability.Wisdom, 10)>]
[<InlineData(Ability.Charisma, 10)>]
let ``GetAbilityScore.It defaults a given ability score to 10 for a given character.`` (ability:Ability, expected:int) =
    let actual =
        Character.Create()
        |> Character.GetAbilityScore ability
    Assert.Equal(expected, actual)

[<Theory>]        
[<InlineData(Ability.Strength, 1, 1)>]
[<InlineData(Ability.Dexterity, 2, 2)>]
[<InlineData(Ability.Constitution, 3, 3)>]
[<InlineData(Ability.Intelligence, 4, 4)>]
[<InlineData(Ability.Wisdom, 5, 5)>]
[<InlineData(Ability.Charisma, 6, 6)>]
[<InlineData(Ability.Strength, 7, 7)>]
[<InlineData(Ability.Dexterity, 8, 8)>]
[<InlineData(Ability.Constitution, 9, 9)>]
[<InlineData(Ability.Intelligence, 10, 10)>]
[<InlineData(Ability.Wisdom, 11, 11)>]
[<InlineData(Ability.Charisma, 12, 12)>]
[<InlineData(Ability.Strength, 13, 13)>]
[<InlineData(Ability.Dexterity, 14, 14)>]
[<InlineData(Ability.Constitution, 15, 15)>]
[<InlineData(Ability.Intelligence, 16, 16)>]
[<InlineData(Ability.Wisdom, 17, 17)>]
[<InlineData(Ability.Charisma, 18, 18)>]
[<InlineData(Ability.Strength, 19, 19)>]
[<InlineData(Ability.Dexterity, 20, 20)>]
let ``SetAbilityScore.It sets a given ability score to a given value for a given character.`` (ability:Ability, assignedValue:int, expectedValue:int) =
    let actual =
        Character.Create()
        |> Character.SetAbilityScore ability assignedValue
        |> Character.GetAbilityScore ability 
    Assert.Equal(expectedValue, actual)
    
[<Theory>]
[<InlineData(Ability.Strength,1,(-5))>]
[<InlineData(Ability.Dexterity,2,(-4))>]
[<InlineData(Ability.Constitution,3,(-4))>]
[<InlineData(Ability.Intelligence,4,(-3))>]
[<InlineData(Ability.Wisdom,5,(-3))>]
[<InlineData(Ability.Charisma,6,(-2))>]
[<InlineData(Ability.Strength,7,(-2))>]
[<InlineData(Ability.Dexterity,8,(-1))>]
[<InlineData(Ability.Constitution,9,(-1))>]
[<InlineData(Ability.Intelligence,10,0)>]
[<InlineData(Ability.Wisdom,11,0)>]
[<InlineData(Ability.Charisma,12,1)>]
[<InlineData(Ability.Strength,13,1)>]
[<InlineData(Ability.Dexterity,14,2)>]
[<InlineData(Ability.Constitution,15,2)>]
[<InlineData(Ability.Intelligence,16,3)>]
[<InlineData(Ability.Wisdom,17,3)>]
[<InlineData(Ability.Charisma,18,4)>]
[<InlineData(Ability.Strength,19,4)>]
[<InlineData(Ability.Dexterity,20,5)>]
let ``GetAbilityScoreModifier.It retrieves the modifier associated with a given ability score for a given character.`` (ability:Ability, assignedValue:int, expectedModifier:int) =
    let actual =
        Character.Create()
        |> Character.SetAbilityScore ability assignedValue
        |> Character.GetAbilityModifier ability 
    Assert.Equal(expectedModifier, actual)
    
[<Theory>]
[<InlineData(12,6)>]
[<InlineData(11,5)>]
[<InlineData(10,5)>]
[<InlineData(9,4)>]
[<InlineData(8,4)>]
[<InlineData(7,3)>]
[<InlineData(6,3)>]
[<InlineData(5,2)>]
[<InlineData(4,2)>]
[<InlineData(3,1)>]
[<InlineData(2,1)>]
[<InlineData(1,1)>]
let ``GetHitPoints.It adds constitution modifier by default.`` (abilityScore: int, expectedHitPoints: int) =
    let actual =
        Character.Create()
        |> Character.SetAbilityScore Ability.Constitution abilityScore
        |> Character.GetHitPoints
    Assert.Equal(expectedHitPoints, actual)      
    
[<Fact>]
let ``GetLevel.It starts the character at first level.`` () =
    let actual = 
        Character.Create()
        |> Character.GetExperienceLevel
    Assert.Equal(1, actual)


[<Theory>]
[<InlineData(0, 1)>]
[<InlineData(999, 1)>]
[<InlineData(1000, 2)>]
[<InlineData(1001, 2)>]
[<InlineData(2000, 3)>]
[<InlineData(3000, 4)>]
let ``GetLevel.It levels up when experience points meet a certain threshhold.`` (experiencePoints: int, expectedLevel:int) =
    let actual = 
        Character.Create()
        |> Character.AddExperiencePoints experiencePoints
        |> Character.GetExperienceLevel
    Assert.Equal(expectedLevel, actual)

[<Theory>]
[<InlineData(0, 10, 5)>]
[<InlineData(1000, 10, 10)>]
[<InlineData(2000, 10, 15)>]
[<InlineData(1000, 8, 8)>]
[<InlineData(1000, 12, 12)>]
[<InlineData(1000, 1, 2)>]
let ``GetHitpoints.It gives an extra 5 plus CON modifier for each level.`` (experiencePoints:int, constitution: int, expectedHitPoints:int) =
    let actual =
        Character.Create()
        |> Character.SetAbilityScore Ability.Constitution constitution
        |> Character.AddExperiencePoints experiencePoints
        |> Character.GetHitPoints
    Assert.Equal(expectedHitPoints, actual)
