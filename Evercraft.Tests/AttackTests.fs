﻿module AttackTests

open Evercraft
open Xunit

[<Theory>]
[<InlineData(0, 1, false, 5, 0)>]
[<InlineData(0, 2, false, 5, 0)>]
[<InlineData(0, 3, false, 5, 0)>]
[<InlineData(0, 4, false, 5, 0)>]
[<InlineData(0, 5, false, 5, 0)>]
[<InlineData(0, 6, false, 5, 0)>]
[<InlineData(0, 7, false, 5, 0)>]
[<InlineData(0, 8, false, 5, 0)>]
[<InlineData(0, 9, false, 5, 0)>]
[<InlineData(0, 10, true, 4, 10)>]
[<InlineData(0, 11, true, 4, 10)>]
[<InlineData(0, 12, true, 4, 10)>]
[<InlineData(0, 13, true, 4, 10)>]
[<InlineData(0, 14, true, 4, 10)>]
[<InlineData(0, 15, true, 4, 10)>]
[<InlineData(0, 16, true, 4, 10)>]
[<InlineData(0, 17, true, 4, 10)>]
[<InlineData(0, 18, true, 4, 10)>]
[<InlineData(0, 19, true, 4, 10)>]
[<InlineData(0, 20, true, 3, 10)>]
[<InlineData(1000, 8, false, 5, 1000)>]
[<InlineData(1000, 9, true, 4, 1010)>]
[<InlineData(2000, 8, false, 5, 2000)>]
[<InlineData(2000, 9, true, 4, 2010)>]
[<InlineData(3000, 7, false, 5, 3000)>]
[<InlineData(3000, 8, true, 4, 3010)>]
let ``Resolve.It returns success and mutated characters when the die roll exceeds the defender's armour class.`` 
        (attackerExperiencePoints:int, dieRoll:int, expectedResult:bool, expectedDefenderHitPoints:int, expectedAttackerExperiencePoints:int) =
    let defender =
        Character.Create()
    let attacker = //"dummy" value
        Character.Create()
        |> Character.AddExperiencePoints attackerExperiencePoints
    let actualResult, updatedAttacker, updatedDefender =
        Attack.Resolve attacker defender dieRoll
    Assert.Equal(expectedResult, actualResult)
    Assert.Equal(expectedDefenderHitPoints, updatedDefender |> Character.GetHitPoints)
    Assert.Equal(expectedAttackerExperiencePoints, updatedAttacker |> Character.GetExperiencePoints)

[<Theory>]
[<InlineData(8,20,true,3, 10)>]
[<InlineData(8,12,true,4, 10)>]
[<InlineData(12,20,true,1, 10)>]
[<InlineData(12,9,true,3, 10)>]
[<InlineData(12,8,false,5, 0)>]
let ``Resolve.It adds strength modifier to attack roll and damage roll.`` 
        (strengthValue: int, 
         dieRoll: int, 
         expectedResult: bool, 
         expectedDefenderHitPoints: int,
         expectedAttackerExperiencePoints: int) =
    let defender =
        Character.Create()
    let attacker =
        Character.Create()
        |> Character.SetAbilityScore Ability.Strength strengthValue
    let actualResult, updatedAttacker, updatedDefender =
        Attack.Resolve attacker defender dieRoll
    Assert.Equal(expectedResult, actualResult)
    Assert.Equal(expectedDefenderHitPoints, updatedDefender |> Character.GetHitPoints)
    Assert.Equal(expectedAttackerExperiencePoints, updatedAttacker |> Character.GetExperiencePoints)

[<Theory>]
[<InlineData(12, 10, false, 5, 0)>]
[<InlineData(12, 11, true, 4, 10)>]
let ``Resolve.It adds dexterity modifier to defender's armour class.`` (dexterityValue: int, dieRoll: int, expectedResult: bool, expectedDefenderHitPoints: int, expectedExperiencePoints) =
    let defender =
        Character.Create()
        |> Character.SetAbilityScore Ability.Dexterity dexterityValue
    let attacker =
        Character.Create()
    let actualResult, updatedAttacker, updatedDefender =
        Attack.Resolve attacker defender dieRoll
    Assert.Equal(expectedResult, actualResult)
    Assert.Equal(expectedDefenderHitPoints, updatedDefender |> Character.GetHitPoints)
    Assert.Equal(expectedExperiencePoints, updatedAttacker |> Character.GetExperiencePoints)
 
    